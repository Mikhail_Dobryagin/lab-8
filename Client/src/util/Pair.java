package util;

import java.io.Serializable;

/**
 * Пара из двух значений
 */
public class Pair<T1, T2> implements Serializable
{
    public static final long serialVersionUID = 9L;
    public T1 first;
    public T2 second;

    public Pair(T1 first, T2 second)
    {
        this.first = first;
        this.second = second;
    }
}
