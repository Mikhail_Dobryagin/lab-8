package util;

import java.util.LinkedList;

public interface CommandExecutor
{
    LinkedList<ErrorReturn> tryToExecute(String[] commandWithArgs);
}
