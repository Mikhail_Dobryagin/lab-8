package util;

public interface Attempt<T>
{
    public ErrorReturn tryAction(T o1, T o2);
}
