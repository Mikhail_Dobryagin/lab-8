package com.company;

import network.PacketCtoS;
import util.ErrorReturn;

import java.util.LinkedList;

/**
 * Интерфейс обобщающий различные команды
 */
public interface Command
{
    /**
     * Выполнить команду, не указывая аргументы
     *
     * @return ErrorReturn<int, String><br/>int-- {0 - OK; 1 - Всё плохо (рекомендуется завершить программу); 2 - Произошла ошибка (Можно продолжить программу)}, String -- описание
     */

    ErrorReturn execute(PacketCtoS outputPacket);

    /**
     * Выполнить команду с указанием ургументов
     *
     * @return ErrorReturn<int, String><br/>int-- {0 - OK; 1 - Всё плохо (рекомендуется завершить программу); 2 - Произошла ошибка (Можно продолжить программу)}, String -- описание
     */
    ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs, PacketCtoS outputPacket);

}
