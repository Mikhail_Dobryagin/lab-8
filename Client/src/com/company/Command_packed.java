package com.company;

public class Command_packed
{
    String name;
    Command commandClient;
    CommandProcessingServerReturn commandFromServer = null;
    int coutOfArgs = 0;
    int coutOfConsArgs = 0;
    boolean hasOutput = false;

    private boolean needId = false;
    private boolean needSpacemarine = false;
    private boolean needString = false;

    public Command_packed(String name, Command command)
    {
        this.name = name;
        this.commandClient = command;
    }

    public Command_packed(String name, Command command, Integer countOfArgs, boolean hasOutput)
    {
        this.name = name;
        this.commandClient = command;
        this.coutOfArgs = countOfArgs;
        this.hasOutput = hasOutput;
    }

    public Command_packed(String name, Command command, Integer countOfArgs, Integer countOfConsArgs, boolean hasOutput)
    {
        this.name = name;
        this.commandClient = command;
        this.coutOfArgs = countOfArgs;
        this.coutOfConsArgs = countOfConsArgs;
        this.hasOutput = hasOutput;
    }

    public Command_packed needId()
    {
        this.needId = true;
        return this;
    }

    public Command_packed needSpacemarine()
    {
        this.needSpacemarine = true;
        return this;
    }

    public Command_packed needString()
    {
        this.needString = true;
        return this;
    }

    public boolean isIdNeeded()
    {
        return this.needId;
    }

    public boolean isSpacemarineNeeded()
    {
        return this.needSpacemarine;
    }

    public boolean isStringNeeded()
    {
        return this.needString;
    }

    public Command_packed setCountOfArgs(int countOfArgs)
    {
        this.coutOfArgs = countOfArgs;
        return this;
    }

    public Command_packed setCountOfConsArgs(int countOfConsArgs)
    {
        this.coutOfConsArgs = countOfConsArgs;
        return this;
    }

    public Command_packed setCommandFromServer(CommandProcessingServerReturn commandFromServer)
    {
        this.commandFromServer = commandFromServer;
        return this;
    }

    public Command_packed setHasOutput(boolean hasOutput)
    {
        this.hasOutput = hasOutput;
        return this;
    }

    public String getName()
    {
        return name;
    }
}
