package com.company;

import network.PacketStoC;


public interface CommandProcessingServerReturn
{
    String execute(PacketStoC argsFromServer);
}
