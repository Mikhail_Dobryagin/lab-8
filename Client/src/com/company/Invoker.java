package com.company;

import network.PacketCtoS;
import network.PacketStoC;
import util.ErrorReturn;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Класс, управляющий выполнением комманд
 */
public class Invoker
{
    /**
     * Словарь из комманд -- <имя комманды, <объект комманды, <количество аргументов <i>общее(может быть больше -- тогда в команду нужно передавать список команд)</i>, <количество аргументов <i>для консоли</i>>>>>
     */
    private final HashMap<String, Command_packed> commandMap = new HashMap<>();

    /**
     * Добавить команду <b>без аргументов</b>
     *
     * @param commandName Название команды
     * @param command     Объект комманды
     */
    public void register(String commandName, Command command)
    {
        commandMap.put(commandName, new Command_packed(commandName, command, 0, 0, false));
    }

    public void register(Command_packed command_packed)
    {
        commandMap.put(command_packed.name, command_packed);
    }

    /**
     * Добавить команду без аргументов в консоли
     *
     * @param commandName Название команды
     * @param command     Объект комманды
     * @param countOfArgs Общее количество аргументов для команды
     */
    public void register(String commandName, Command command, Integer countOfArgs)
    {
        commandMap.put(commandName, new Command_packed(commandName, command, countOfArgs, 0, false));
    }

    public void register(String commandName, Command command, Integer countOfArgs, boolean hasOutput)
    {
        commandMap.put(commandName, new Command_packed(commandName, command, countOfArgs, 0, hasOutput));
    }

    /**
     * Добавить команду
     *
     * @param commandName        Название команды
     * @param command            Объект команды
     * @param countOfArgs        Общее количество аргументов для команды (сколько минимум должны подать из списка команд)
     * @param countOfConsoleArgs Количество аргументов, вводимых из терминала
     */
    public void register(String commandName, Command command, Integer countOfArgs, Integer countOfConsoleArgs)
    {
        commandMap.put(commandName, new Command_packed(commandName, command, countOfArgs, countOfConsoleArgs, false));
    }

    public void register(String commandName, Command command, Integer countOfArgs, Integer countOfConsoleArgs, boolean hasOutput)
    {
        commandMap.put(commandName, new Command_packed(commandName, command, countOfArgs, countOfConsoleArgs, hasOutput));
    }

    public void registerSecondCommand(String commandName, CommandProcessingServerReturn command)
    {
        try {
            commandMap.get(commandName).commandFromServer = command;
        } catch (NullPointerException ignored) {

        }
    }

    /**
     * Исполнить команду, указав аргументы
     *
     * @param commandName Название команды
     * @param args        Массив аругментов
     */
    public ErrorReturn execute(String commandName, Object[] args, LinkedList<String> listWithArgs, PacketCtoS outputPacket)
    {
        if (!isInInvoker(commandName))
            return new ErrorReturn(2, "Команды \"" + commandName + "\" не существует");

        outputPacket.setCommandName(commandName);

        outputPacket.setUser(Main.user);

        ErrorReturn error = commandMap.get(commandName).commandClient.execute(args, listWithArgs, outputPacket);
        return error == null ? new ErrorReturn(2, "Неверно введены аргументы") : error;
    }

    /**
     * Исполнить команду, не указывая аргументы
     *
     * @param commandName Название команды
     */

    public ErrorReturn execute(String commandName, PacketCtoS outputPacket)
    {
        if (!isInInvoker(commandName))
            return new ErrorReturn(2, "Команды \"" + commandName + "\" не существует");

        outputPacket.setCommandName(commandName);

        outputPacket.setUser(Main.user);

        ErrorReturn error = commandMap.get(commandName).commandClient.execute(outputPacket);
        return error == null ? new ErrorReturn(2, "Неверно введены аргументы") : error;
    }

    public String processAnswerFromServer(String commandName, PacketStoC inputPacket)
    {
        if (!isInInvoker(commandName))
            return "";

        return commandMap.get(commandName).commandFromServer.execute(inputPacket);
    }

    /**
     * Проверить, добавлена ли указанная команда
     *
     * @param commandName Название команды
     */
    public boolean isInInvoker(String commandName)
    {
        return !(commandMap.get(commandName) == null);
    }

    /**
     * Получить количество аргументов команды
     *
     * @param commandName Название команды
     * @return -1, если такой команды не существует
     */
    public int getCountOfArgs(String commandName)
    {
        if (!isInInvoker(commandName))
            return -1;
        return commandMap.get(commandName).coutOfArgs;
    }

    /**
     * Получить количество аргументов команды, вводимых из терминала
     *
     * @param commandName Название команды
     * @return -1, если такой команды не существует
     */
    public int getCountOfConsoleArgs(String commandName)
    {
        if (!isInInvoker(commandName))
            return -1;
        return commandMap.get(commandName).coutOfConsArgs;
    }

    public Boolean hasOutput(String commandName)
    {
        if (!isInInvoker(commandName))
            return null;

        return commandMap.get(commandName).hasOutput;
    }

    public Boolean hasAnswer(String commandName)
    {
        return isInInvoker(commandName) ? commandMap.get(commandName).commandFromServer != null : null;
    }

    public Command_packed getCommand(String s)
    {
        return commandMap.get(s);
    }
}
