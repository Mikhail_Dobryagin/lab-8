package com.company;

import commands.client.*;
import commands.server.CommandSout;
import controllers.ControllerLogIn;
import controllers.ControllerMainScene;
import database.CheckUserResult;
import database.User;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import network.Connection;
import network.PacketCtoS;
import network.PacketStoC;
import util.ColAnsi;
import util.ErrorReturn;

import java.io.IOException;
import java.nio.channels.NonReadableChannelException;
import java.nio.channels.NonWritableChannelException;
import java.nio.channels.NotYetConnectedException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

public class Main extends Application
{
    static User user;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        ColAnsi.init();

        LinkedList<String> list = new LinkedList<>();

        Invoker invoker = new Invoker();

        registerCommands(invoker, list);

        HashSet<String> commandsSet = new HashSet<>();

        Connection connection = null;

        String[] args = getParameters().getRaw().toArray(new String[0]);

        try {
            if (args.length >= 2)
                connection = new Connection(args[0], Integer.parseInt(args[1]));
            if (args.length == 1)
                connection = new Connection(Integer.parseInt(args[0]));
        } catch (IllegalArgumentException | IllegalStateException e) {
            System.out.println(ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            return;
        }

        if (args.length == 0)
            connection = new Connection();

        user = new User();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("../../fxml/logIn.fxml"));

        Parent root = loader.load();


        primaryStage.setTitle("Вход");
        Scene scene = new Scene(root, 600, 400);


//        Image image = new Image(getClass().getResourceAsStream("icon.png"));
//        primaryStage.getIcons().add(image);

        primaryStage.setResizable(false);
        primaryStage.setScene(scene);

        Stage stage = new Stage();

        FXMLLoader loader2 = new FXMLLoader();
        loader2.setLocation(getClass().getResource("../../fxml/mainScene.fxml"));

        Parent root2 = loader2.load();

        Scene scene2 = new Scene(root2, 600, 400);

        stage.initStyle(StageStyle.UNDECORATED);
        stage.setResizable(false);

        stage.setScene(scene2);
        ControllerMainScene controllerMainScene = loader2.getController();

        ControllerLogIn controllerLogIn = loader.getController();

        {
            final Connection finalConnection = connection;
            controllerLogIn.setLoginAction((login, password) -> {
                ErrorReturn errorReturn = logIn(finalConnection, login, password);
                if (errorReturn.getCode() == 0)
                    actionOnSuccessfulEnter(primaryStage, controllerMainScene, stage, invoker, list, commandsSet, finalConnection);

                return errorReturn;

            });
            controllerLogIn.setRegisterAction((login, password) -> {
                ErrorReturn errorReturn = registerUser(finalConnection, login, password);
                if (errorReturn.getCode() == 0)
                    actionOnSuccessfulEnter(primaryStage, controllerMainScene, stage, invoker, list, commandsSet, finalConnection);

                return errorReturn;

            });

        }

        primaryStage.show();
    }

    private static ErrorReturn actionWithoutError(Invoker invoker, String commandName, PacketCtoS outputPacket, ErrorReturn error, Connection connection)
    {

        if (error.getStatus() != null)
            System.out.println(ColAnsi.ANSI_GREEN + error.getStatus() + ColAnsi.ANSI_RESET);

        if (!commandName.equals("execute_script")) {
            try {
                connection.setChannel();
            } catch (IOException e) {
                return new ErrorReturn(1, "Невозможно присоединиться к серверу");
            }

            try {
                connection.send(outputPacket);
            } catch (IOException e) {
                return new ErrorReturn(1, "Произошла ошибка вывода");
            } catch (NotYetConnectedException e) {
                return new ErrorReturn(1, "Канал не присоединён к серверу");
            } catch (NonWritableChannelException e) {
                return new ErrorReturn(1, "Невозможно осуществить отправку данных на сервер");
            }

            try {
                if (invoker.hasAnswer(commandName))
                    return new ErrorReturn(0, invoker.processAnswerFromServer(commandName, connection.getData()));
            } catch (IOException e) {
                return new ErrorReturn(1, "Произошла ошибка при чтении данных с сервера");
            } catch (NonReadableChannelException e) {
                return new ErrorReturn(1, "Невозможно получить данные с сервера");
            } catch (NotYetConnectedException e) {
                return new ErrorReturn(1, "Канал не присоединён к серверу");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            try {
                connection.closeChannel();
            } catch (IOException e) {
                return new ErrorReturn(1, e.getMessage());
            }
        }

        return ErrorReturn.OK();
    }

    private static void registerCommands(Invoker invoker, LinkedList<String> list)
    {
        invoker.register(new Command_packed("help", new CommandHelp()));
        invoker.register(new Command_packed("info", new CommandInfo()));
        invoker.register(new Command_packed("show", new commands.client.CommandShow()));
        invoker.register(new Command_packed("add", new CommandAdd()).setCountOfArgs(8).setHasOutput(true).needSpacemarine());
        invoker.register(new Command_packed("add_if_min", new CommandAdd()).setCountOfArgs(8).setHasOutput(true).needSpacemarine());
        invoker.register(new Command_packed("update", new CommandUpdate()).setCountOfArgs(9).setCountOfConsArgs(1).setHasOutput(true).needId().needSpacemarine());
        invoker.register(new Command_packed("remove_by_id", new CommandRemoveById()).setCountOfArgs(1).setCountOfConsArgs(1).setHasOutput(true).needId());
        invoker.register(new Command_packed("clear", new CommandClear()));
        invoker.register(new Command_packed("execute_script", new CommandExecuteScript(list)).setCountOfArgs(1).setCountOfConsArgs(1));
        invoker.register(new Command_packed("exit", new CommandExit()));
        invoker.register(new Command_packed("remove_first", new CommandRemoveFirst()));
        invoker.register(new Command_packed("remove_head", new CommandRemoveHead()));
        invoker.register(new Command_packed("filter_contains_name", new CommandFilterContainsName()).setCountOfArgs(1).setCountOfConsArgs(1).setHasOutput(true).needString());
        invoker.register(new Command_packed("filter_starts_with_name", new CommandFilterStartsWithName()).setCountOfArgs(1).setCountOfConsArgs(1).setHasOutput(true).needString());
        invoker.register(new Command_packed("print_descending", new CommandPrintDescending()));

        invoker.registerSecondCommand("help", new CommandSout());
        invoker.registerSecondCommand("info", new CommandSout());
        invoker.registerSecondCommand("show", new commands.server.CommandShow());
        invoker.registerSecondCommand("add", new CommandSout());
        invoker.registerSecondCommand("add_if_min", new CommandSout());
        invoker.registerSecondCommand("update", new CommandSout());
        invoker.registerSecondCommand("remove_by_id", new CommandSout());
        invoker.registerSecondCommand("clear", new CommandSout());
        invoker.registerSecondCommand("remove_first", new CommandSout());
        invoker.registerSecondCommand("remove_head", new commands.server.CommandShow());
        invoker.registerSecondCommand("filter_contains_name", new commands.server.CommandShow());
        invoker.registerSecondCommand("filter_starts_with_name", new commands.server.CommandShow());
        invoker.registerSecondCommand("print_descending", new commands.server.CommandShow());
    }

    private static ErrorReturn logIn(Connection connection, String login, String password)
    {
        User subUser = new User(login, password);

        if (subUser.getLogin() == null)
            return new ErrorReturn(1, "Некорректный логин");

        subUser.encodePassword();

        try {
            connection.setChannel();
            PacketCtoS netArgs = new PacketCtoS("check_user", subUser);

            connection.send(netArgs);

            PacketStoC answer = connection.getData();

            ErrorReturn errorReturn = answer.getErrorReturn();

            if (errorReturn.getCode() != 0)
                return errorReturn;

            CheckUserResult result = answer.getResult();

            connection.closeChannel();

            if (result == CheckUserResult.RIGHT) {
                user = subUser;
                return ErrorReturn.OK();
            }

            return new ErrorReturn(1, result == CheckUserResult.WRONG_PASSWORD ? "Неверный пароль" : "Пользователя с таким логином не существует");

        } catch (IOException | ClassNotFoundException e) {
            return new ErrorReturn(1, e.getMessage());
        }
    }

    private static ErrorReturn registerUser(Connection connection, String login, String password)
    {
        User subUser = new User(login, password);
        subUser.encodePassword();


        try {
            PacketCtoS netArgs = new PacketCtoS("check_user", subUser);
            connection.setChannel();
            connection.send(netArgs);

            PacketStoC answer = connection.getData();

            ErrorReturn errorReturn = answer.getErrorReturn();

            if (errorReturn.getCode() > 0)
                return errorReturn;

            CheckUserResult result = answer.getResult();

            if (result != CheckUserResult.WRONG_USER)
                return new ErrorReturn(1, "Пользователь с таким логином уже занят");

            connection.closeChannel();

            connection.setChannel();
            netArgs = new PacketCtoS("register", subUser);
            connection.send(netArgs);

            connection.closeChannel();
            user = subUser;
            return ErrorReturn.OK();

        } catch (IOException | ClassNotFoundException e) {
            return new ErrorReturn(1, e.getMessage());
        }

    }

    private void actionOnSuccessfulEnter(Stage primaryStage, ControllerMainScene controllerMainScene, Stage stage, Invoker invoker, LinkedList<String> list, HashSet<String> commandsSet, Connection finalConnection)
    {
        primaryStage.hide();
        controllerMainScene.init(user);
        controllerMainScene.addCommandGetter(invoker::getCommand);
        controllerMainScene.registerCommandExecutor(commandWithArgs -> {
            list.addAll(Arrays.asList(commandWithArgs));

            LinkedList<ErrorReturn> errorReturns = new LinkedList<>();

            while (true) {
                if (list.isEmpty())
                    return errorReturns;

                if (list.getFirst().equals("&&&")) {
                    list.removeFirst();
                    continue;
                }

                if (list.getFirst().contains("&&&")) {
                    list.clear();
                    errorReturns.add(new ErrorReturn(1, "Неверно введены аргументы"));
                    return errorReturns;
                }

                String[] commandNameWithArg = list.getFirst().split(" ", 2);
                list.removeFirst();

                String commandName = commandNameWithArg[0];

                if (!list.isEmpty() && commandName.equals("execute_script")) {
                    if (commandsSet.contains(list.getFirst())) {
                        list.clear();
                        errorReturns.add(new ErrorReturn(1, "У вас нашли рекурсию\nЗдоровья погибшим"));
                        return errorReturns;
                    }

                    commandsSet.add(list.getFirst());
                }


                int countOfArgs = invoker.getCountOfArgs(commandName);
                int countOfConsoleArgs = invoker.getCountOfConsoleArgs(commandName);

                if (countOfArgs < 0) {
                    list.clear();
                    errorReturns.add(new ErrorReturn(1, "Команды " + commandName + " не существует"));
                    return errorReturns;
                }

                if (countOfArgs - countOfConsoleArgs > list.size()) {
                    list.clear();
                    errorReturns.add(new ErrorReturn(1, "Мало аргументов для вызова функции"));
                    return errorReturns;
                }

                ErrorReturn error = null;

                Object[] argsForCommand = new Object[countOfArgs];
                for (int i = 0; i < countOfArgs; i++) {
                    if (i == 0 && commandNameWithArg.length == 2)
                        argsForCommand[i] = commandNameWithArg[1];
                    else {
                        argsForCommand[i] = list.getFirst();
                        list.removeFirst();
                    }

                    if (((String) argsForCommand[i]).contains("&&&")) {
                        error = new ErrorReturn(2, "Неверно введены аргументы" + "\n" + argsForCommand[i]);
                        break;
                    }
                }

                if (error != null) {
                    list.clear();
                    errorReturns.add(new ErrorReturn(1, error.getStatus()));
                    return errorReturns;
                }

                PacketCtoS outputPacket = new PacketCtoS();

                error = countOfArgs == 0 ? invoker.execute(commandName, outputPacket) : invoker.execute(commandName, argsForCommand, list, outputPacket);

                if (error.getCode() != 0) {
                    list.clear();
                    errorReturns.add(error);
                    return errorReturns;
                }

                errorReturns.add(actionWithoutError(invoker, commandName, outputPacket, error, finalConnection));
            }
        });
        controllerMainScene.setSpacemarinesGetter(() -> {
            try{
                finalConnection.setChannel();
            }catch (IOException e)
            {
                return null;
            }

            PacketCtoS packetCtoS = new PacketCtoS();
            packetCtoS.setCommandName("show");
            packetCtoS.setUser(user);

            try {
                finalConnection.send(packetCtoS);
                return finalConnection.getData().getSpaceMarines();
            }catch (IOException | ClassNotFoundException e)
            {
                return null;
            }

        });
        stage.show();
    }

}
