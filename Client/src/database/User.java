package database;

import java.io.Console;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class User implements Serializable
{
    private String login;
    byte[] password;
    public static final long serialVersionUID = 10L;

    public User(String login, byte[] password)
    {
        this.login = login;
        this.password = password;
    }

    public User(String login, String password)
    {
        this.login = login;
        this.password = password.getBytes();
    }

    public User()
    {
        this.login = null;
        this.password = null;
    }

    public String getLogin()
    {
        return login;
    }

    public byte[] getPassword()
    {
        return password;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public void setPassword(String login)
    {
        this.login = login;
    }

    public void encodePassword()
    {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            password = md.digest(password);
        } catch (NoSuchAlgorithmException ignored) {

        }
    }

    public static User scanUser(Console console)
    {
        System.out.print("Введите логин: ");
        String login = console.readLine();

        if (login == null)
            return new User();

        System.out.print("Введите пароль: ");

        String password;

        try {
            password = new String(console.readPassword());
        } catch (NullPointerException e) {
            password = " ";
        }

        return new User(login, password.getBytes());
    }
}
