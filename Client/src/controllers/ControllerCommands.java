package controllers;

import com.company.Command_packed;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import util.CommandExecutor;
import util.ErrorReturn;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Function;


public class ControllerCommands
{
    @FXML
    ComboBox<String> commandName;
    @FXML
    Button executeButton;
    @FXML
    TextField idField, inputStringField;

    @FXML
    TextField nameField, xField, yField, healthField, commanderNameField, parentLegionField;
    @FXML
    ComboBox<String> roleField, weaponField, meleeWeaponField;
    @FXML
    CheckBox isChapterCheckBox;

    @FXML
    VBox spacemarineBox;

    @FXML
    TextArea outputField;

    @FXML
    Button chooseFileButton;

    Function<String, Command_packed> commandGetter;
    CommandExecutor commandExecutor;


    public ControllerCommands(Tab commandTab)
    {
        this.commandName = (ComboBox<String>) (commandTab.getContent().lookup("#commandName"));
        this.executeButton = (Button) (commandTab.getContent().lookup("#executeButton"));
        this.idField = (TextField) (commandTab.getContent().lookup("#idField"));
        this.inputStringField = (TextField) (commandTab.getContent().lookup("#inputStringField"));
        this.chooseFileButton = (Button) (commandTab.getContent().lookup("#chooseFileButton"));

        this.spacemarineBox = (VBox) (commandTab.getContent().lookup("#spacemarineBox"));

        this.nameField = (TextField) (commandTab.getContent().lookup("#nameField"));
        this.xField = (TextField) (commandTab.getContent().lookup("#xField"));
        this.yField = (TextField) (commandTab.getContent().lookup("#yField"));
        this.healthField = (TextField) (commandTab.getContent().lookup("#healthField"));
        this.roleField = (ComboBox<String>) (commandTab.getContent().lookup("#roleField"));
        this.weaponField = (ComboBox<String>) (commandTab.getContent().lookup("#weaponField"));
        this.meleeWeaponField = (ComboBox<String>) (commandTab.getContent().lookup("#meleeWeaponField"));
        this.isChapterCheckBox = (CheckBox) (commandTab.getContent().lookup("#isChapterCheckBox"));
        this.commanderNameField = (TextField) (commandTab.getContent().lookup("#commanderNameField"));
        this.parentLegionField = (TextField) (commandTab.getContent().lookup("#parentLegionField"));

        this.outputField = (TextArea) (commandTab.getContent().lookup("#outputField"));


        idField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals(""))
                return;

            try {
                if (Long.parseLong(newValue) <= 0L)
                    idField.setText(oldValue);
            } catch (NumberFormatException e) {
                idField.setText(oldValue);
            }
        });
        xField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("") || newValue.equals("-"))
                return;

            try {
                if (Float.parseFloat(newValue) > 594.0f)
                    xField.setText(oldValue);
            } catch (NumberFormatException e) {
                xField.setText(oldValue);
            }
        });
        yField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("") || newValue.equals("-"))
                return;

            try {
                if (Long.parseLong(newValue) <= -746)
                    yField.setText(oldValue);
            } catch (NumberFormatException e) {
                yField.setText(oldValue);
            }
        });
        healthField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals(""))
                return;

            try {
                if (Double.parseDouble(newValue) <= 0)
                    healthField.setText(oldValue);
            } catch (NumberFormatException e) {
                healthField.setText(oldValue);
            }
        });

        isChapterCheckBox.setOnAction(event -> {
            commanderNameField.setVisible(isChapterCheckBox.isSelected());
            parentLegionField.setVisible(isChapterCheckBox.isSelected());
        });

        commandName.setOnAction(event -> {
            deactivateAllFields();

            if (commandName.getValue().equals(""))
                return;

            executeButton.setDisable(false);

            Command_packed command_packed = commandGetter.apply(commandName.getValue());

            if (command_packed.getName().equals("execute_script"))
                chooseFileButton.setVisible(true);


            if (command_packed.isIdNeeded())
                idField.setVisible(true);
            if (command_packed.isSpacemarineNeeded())
                spacemarineBox.setVisible(true);
            if (command_packed.isStringNeeded())
                inputStringField.setVisible(true);
        });

        chooseFileButton.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showOpenDialog(chooseFileButton.getScene().getWindow());
            if (file != null) {
                inputStringField.setText(file.getAbsolutePath());
                chooseFileButton.setText(file.getAbsolutePath());
            }
        });

        executeButton.setOnAction(event -> {

            String[] commandWithArgs = collectCommandWithArgs();

            deactivateAllFields();

            for (ErrorReturn errorReturn : commandExecutor.tryToExecute(commandWithArgs)) {
                if (errorReturn.getCode() == 0) {
                    if (errorReturn.getStatus() != null)
                        outputField.appendText(errorReturn.getStatus());
                    continue;
                }

                ((Stage) commandTab.getTabPane().getScene().getWindow()).setIconified(true);

                Stage errorStage = new Stage();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../fxml/error.fxml"));

                Parent root = null;
                try {
                    root = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Scene scene = new Scene(root, 482, 225);

                errorStage.initStyle(StageStyle.UNDECORATED);
                errorStage.setResizable(false);
                errorStage.setScene(scene);

                ControllerError controllerError = loader.getController();
                controllerError.init();
                controllerError.setError(errorReturn);

                errorStage.show();
                commandName.getSelectionModel().select(0);
                return;
            }

            commandName.getSelectionModel().select(0);
        });

    }

    public void setCommandGetter(Function<String, Command_packed> commandGetter)
    {
        this.commandGetter = commandGetter;
    }

    public void setCommandExecutor(CommandExecutor commandExecutor)
    {
        this.commandExecutor = commandExecutor;
    }

    private void deactivateAllFields()
    {
        isChapterCheckBox.setSelected(false);
        commanderNameField.setVisible(false);
        parentLegionField.setVisible(false);

        idField.setVisible(false);
        inputStringField.setVisible(false);
        chooseFileButton.setVisible(false);

        executeButton.setDisable(true);
        spacemarineBox.setVisible(false);

    }

    private String[] collectCommandWithArgs()
    {
        if (!spacemarineBox.isVisible()) {
            return new String[]{commandName.getValue() + (idField.isVisible() ? " " + idField.getText() : "") + (inputStringField.isVisible() || chooseFileButton.isVisible() ? " " + inputStringField.getText() : "")};
        }


        String[] args = new String[]{
                commandName.getValue() + (idField.isVisible() ? " " + idField.getText() : "") + (inputStringField.isVisible() ? " " + inputStringField.getText() : ""),
                nameField.getText(),
                xField.getText(),
                yField.getText(),
                healthField.getText(),
                roleField.getValue(),
                weaponField.getValue(),
                meleeWeaponField.getValue(),
                isChapterCheckBox.isSelected() ? "+" : "-",
                commanderNameField.getText(),
                parentLegionField.getText()
        };

        if(isChapterCheckBox.isSelected())
            return args;

        return Arrays.copyOfRange(args, 0, args.length-2);

    }
}
