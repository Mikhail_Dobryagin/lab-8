package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import spacemarine.*;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Locale;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ControllerTable
{
    @FXML
    TableView<SeparatedSpaceMarineWithOwner> tableView;
    @FXML
    TableColumn<SeparatedSpaceMarineWithOwner, String> nameColumn, commanderNameColumn, parentLegionColumn, ownerColumn;
    @FXML
    TableColumn<SeparatedSpaceMarineWithOwner, Float> xColumn;
    @FXML
    TableColumn<SeparatedSpaceMarineWithOwner, Long> yColumn;
    @FXML
    TableColumn<SeparatedSpaceMarineWithOwner, Double> healthColumn;
    @FXML
    TableColumn<SeparatedSpaceMarineWithOwner, AstartesCategory> categotyColumn;
    @FXML
    TableColumn<SeparatedSpaceMarineWithOwner, Weapon> weaponColumn;
    @FXML
    TableColumn<SeparatedSpaceMarineWithOwner, MeleeWeapon> meleeWeaponColumn;
    @FXML
    TableColumn<SeparatedSpaceMarineWithOwner, LocalDate> creationDateColumn;

    //Filters + reset_button
    @FXML
    TextField nameFilter, commanderNameFilter, parentLegionNameFilter, xFilterLeft, xFilterRight, yFilterLeft, yFilterRight, healthFilterLeft, healthFilterRight;
    @FXML
    VBox categoryFilterBox, weaponFilterBox, meleeWeaponFilterBox;

    @FXML
    Button resetFiltersButton;


    public ControllerTable(Tab tableTab)
    {
        this.tableView = (TableView<SeparatedSpaceMarineWithOwner>) (tableTab.getContent().lookup("#tableView"));

        tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue == null)
                return;

            tableTab.getTabPane().getSelectionModel().select(0);

            ((ComboBox<String>)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#commandName")).setValue("update");


            ((TextField)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#idField")).setText(newValue.getId().toString());

            ((TextField)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#nameField")).setText(newValue.getName());

            ((TextField)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#xField")).setText(Float.toString(newValue.getX()));
            ((TextField)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#yField")).setText(newValue.getY().toString());
            ((TextField)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#healthField")).setText(Double.toString(newValue.getHealth()));
            ((ComboBox)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#roleField")).setValue(newValue.getCategory().toString());
            ((ComboBox)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#weaponField")).setValue(newValue.getWeaponType().toString());
            ((ComboBox)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#meleeWeaponField")).setValue(newValue.getMeleeWeapon().toString());

            ((CheckBox)tableTab.getTabPane().getTabs().get(0).getContent().lookup("#isChapterCheckBox")).setSelected(!newValue.getChapterName().equals(""));
            if(!newValue.getChapterName().equals(""))
            {
                ((TextField) tableTab.getTabPane().getTabs().get(0).getContent().lookup("#commanderNameField")).setText(newValue.getChapterName());
                tableTab.getTabPane().getTabs().get(0).getContent().lookup("#commanderNameField").setVisible(true);
                ((TextField) tableTab.getTabPane().getTabs().get(0).getContent().lookup("#parentLegionField")).setText(newValue.getParentLegion());
                tableTab.getTabPane().getTabs().get(0).getContent().lookup("#parentLegionField").setVisible(true);
            }
        });

        {

            this.nameColumn = (TableColumn<SeparatedSpaceMarineWithOwner, String>) tableView.getColumns().stream().filter(column -> column.getId().equals("nameColumn")).findAny().get();
            this.xColumn = (TableColumn<SeparatedSpaceMarineWithOwner, Float>) tableView.getColumns().stream().filter(column -> column.getId().equals("xColumn")).findAny().get();
            this.yColumn = (TableColumn<SeparatedSpaceMarineWithOwner, Long>) tableView.getColumns().stream().filter(column -> column.getId().equals("yColumn")).findAny().get();
            this.healthColumn = (TableColumn<SeparatedSpaceMarineWithOwner, Double>) tableView.getColumns().stream().filter(column -> column.getId().equals("healthColumn")).findAny().get();
            this.categotyColumn = (TableColumn<SeparatedSpaceMarineWithOwner, AstartesCategory>) tableView.getColumns().stream().filter(column -> column.getId().equals("categoryColumn")).findAny().get();
            this.weaponColumn = (TableColumn<SeparatedSpaceMarineWithOwner, Weapon>) tableView.getColumns().stream().filter(column -> column.getId().equals("weaponColumn")).findAny().get();
            this.meleeWeaponColumn = (TableColumn<SeparatedSpaceMarineWithOwner, MeleeWeapon>) tableView.getColumns().stream().filter(column -> column.getId().equals("meleeWeaponColumn")).findAny().get();
            this.commanderNameColumn = (TableColumn<SeparatedSpaceMarineWithOwner, String>) tableView.getColumns().stream().filter(column -> column.getId().equals("commanderNameColumn")).findAny().get();
            this.parentLegionColumn = (TableColumn<SeparatedSpaceMarineWithOwner, String>) tableView.getColumns().stream().filter(column -> column.getId().equals("parentLegionColumn")).findAny().get();
            this.ownerColumn = (TableColumn<SeparatedSpaceMarineWithOwner, String>) tableView.getColumns().stream().filter(column -> column.getId().equals("ownerColumn")).findAny().get();
            this.creationDateColumn = (TableColumn<SeparatedSpaceMarineWithOwner, LocalDate>) tableView.getColumns().stream().filter(column -> column.getId().equals("dateColumn")).findAny().get();


            nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
            xColumn.setCellValueFactory(new PropertyValueFactory<>("x"));
            yColumn.setCellValueFactory(new PropertyValueFactory<>("y"));
            healthColumn.setCellValueFactory(new PropertyValueFactory<>("health"));
            categotyColumn.setCellValueFactory(new PropertyValueFactory<>("category"));
            weaponColumn.setCellValueFactory(new PropertyValueFactory<>("weaponType"));
            meleeWeaponColumn.setCellValueFactory(new PropertyValueFactory<>("meleeWeapon"));
            commanderNameColumn.setCellValueFactory(new PropertyValueFactory<>("chapterName"));
            parentLegionColumn.setCellValueFactory(new PropertyValueFactory<>("parentLegion"));
            ownerColumn.setCellValueFactory(new PropertyValueFactory<>("owner"));
            creationDateColumn.setCellValueFactory(new PropertyValueFactory<>("creationDate"));
        }

        {

            ScrollPane scrollPane = (ScrollPane) (tableTab.getContent().lookup("#scrollPane"));
            TreeView treeView = (TreeView) (scrollPane.getContent().lookup("#filterTreeView"));

            this.nameFilter = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(0)).getChildren().get(0)).getGraphic().lookup("#nameFilter");

            this.xFilterLeft = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(1)).getChildren().get(0)).getGraphic().lookup("#xFilterLeft");
            this.xFilterRight = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(1)).getChildren().get(0)).getGraphic().lookup("#xFilterRight");
            this.yFilterLeft = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(1)).getChildren().get(0)).getGraphic().lookup("#yFilterLeft");
            this.yFilterRight = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(1)).getChildren().get(0)).getGraphic().lookup("#yFilterRight");
            this.healthFilterLeft = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(2)).getChildren().get(0)).getGraphic().lookup("#healthFilterLeft");
            this.healthFilterRight = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(2)).getChildren().get(0)).getGraphic().lookup("#healthFilterRight");
            this.commanderNameFilter = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(6)).getChildren().get(0)).getGraphic().lookup("#commanderNameFilter");
            this.parentLegionNameFilter = (TextField) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(6)).getChildren().get(0)).getGraphic().lookup("#parentLegionNameFilter");

            this.categoryFilterBox = (VBox) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(3)).getChildren().get(0)).getGraphic().lookup("#categoryFilterBox");
            this.weaponFilterBox = (VBox) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(4)).getChildren().get(0)).getGraphic().lookup("#weaponFilterBox");
            this.meleeWeaponFilterBox = (VBox) ((TreeItem) ((TreeItem) treeView.getTreeItem(0).getChildren().get(5)).getChildren().get(0)).getGraphic().lookup("#meleeWeaponFilterBox");

            this.resetFiltersButton = (Button) (tableTab.getContent().lookup("#resetFiltersButton"));

            this.resetFiltersButton.setOnAction(event -> {
                nameFilter.setText("");
                xFilterLeft.setText("");
                xFilterRight.setText("");
                yFilterLeft.setText("");
                yFilterRight.setText("");
                healthFilterLeft.setText("");
                healthFilterRight.setText("");

                commanderNameFilter.setText("");
                parentLegionNameFilter.setText("");

                categoryFilterBox.getChildren().stream().forEach(x -> ((CheckBox) x).setSelected(true));
                weaponFilterBox.getChildren().stream().forEach(x -> ((CheckBox) x).setSelected(true));
                meleeWeaponFilterBox.getChildren().stream().forEach(x -> ((CheckBox) x).setSelected(true));

                treeView.getTreeItem(0).getChildren().forEach(x -> ((TreeItem) x).setExpanded(false));
            });
        }


    }

    public void update(LinkedList<SpaceMarineWithOwner> spaceMarineWithOwners)
    {
        LinkedList<SeparatedSpaceMarineWithOwner> spacemarines = new LinkedList<>();

        spaceMarineWithOwners.stream().map(SeparatedSpaceMarineWithOwner::new)
                .filter(sm -> sm.getName().matches(".*" + nameFilter.getText() + ".*"))
                .filter(sm -> (xFilterLeft.getText().equals("") || sm.getX() >= Float.parseFloat(xFilterLeft.getText())) && (xFilterRight.getText().equals("") || sm.getX() <= Float.parseFloat(xFilterRight.getText())))
                .filter(sm -> (yFilterLeft.getText().equals("") || sm.getY() >= Long.parseLong(yFilterLeft.getText())) && (yFilterRight.getText().equals("") || sm.getY() <= Long.parseLong(yFilterRight.getText())))
                .filter(sm -> (healthFilterLeft.getText().equals("") || sm.getHealth() >= Double.parseDouble(healthFilterLeft.getText())) && (healthFilterRight.getText().equals("") || sm.getHealth() <= Double.parseDouble(healthFilterRight.getText())))
                .filter(sm -> sm.getChapterName().matches(".*" + commanderNameFilter.getText() + ".*"))
                .filter(sm -> sm.getParentLegion().matches(".*" + parentLegionNameFilter.getText() + ".*"))
                .filter(sm -> !categoryFilterBox.getChildren().stream().map(x -> ((CheckBox) x)).filter(box -> !box.isSelected()).map(box -> AstartesCategory.valueOf(box.getText().replaceAll(" ", "_").toUpperCase(Locale.ROOT))).collect(Collectors.toSet()).contains(sm.getCategory()))
                .filter(sm -> !weaponFilterBox.getChildren().stream().map(x -> ((CheckBox) x)).filter(box -> !box.isSelected()).map(box -> Weapon.valueOf(box.getText().replaceAll(" ", "_").toUpperCase(Locale.ROOT))).collect(Collectors.toSet()).contains(sm.getWeaponType()))
                .filter(sm -> !meleeWeaponFilterBox.getChildren().stream().map(x -> ((CheckBox) x)).filter(box -> !box.isSelected()).map(box -> MeleeWeapon.valueOf(box.getText().replaceAll(" ", "_").toUpperCase(Locale.ROOT))).collect(Collectors.toSet()).contains(sm.getMeleeWeapon()))
                .forEach(spacemarines::add);


        ObservableList<SeparatedSpaceMarineWithOwner> spacemarinesForTable = FXCollections.observableArrayList(spacemarines);

        TableColumn sortColumn = null;
        TableColumn.SortType sortType = null;

        if (tableView.getSortOrder().size() > 0) {
            sortColumn = tableView.getSortOrder().get(0);
            sortType = sortColumn.getSortType();
        }

        if (spacemarinesForTable.size() != tableView.getItems().size()) {
            tableView.setItems(spacemarinesForTable);
            if (sortColumn != null) {
                tableView.getSortOrder().add(sortColumn);
                sortColumn.setSortType(sortType);
                sortColumn.setSortable(true);
            }
            return;
        }

        for (SeparatedSpaceMarineWithOwner sm : spacemarinesForTable)
            if (!tableView.getItems().contains(sm)) {
                tableView.setItems(spacemarinesForTable);
                if (sortColumn != null) {
                    tableView.getSortOrder().add(sortColumn);
                    sortColumn.setSortType(sortType);
                    sortColumn.setSortable(true);
                }
                return;
            }


    }
}
