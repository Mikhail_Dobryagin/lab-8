package controllers;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ArcType;
import spacemarine.SpaceMarineWithOwner;

import java.util.*;
import java.util.function.Supplier;

public class ControllerMap
{
    Pane fieldPane;
    Pane minusButton, plusButton;
    private Supplier<LinkedList<SpaceMarineWithOwner>> spacemarinesGetter;

    HashMap<SpaceMarineWithOwner, Canvas> includedSpaceMarines;

    double minXDivisionValue = -20, maxXDivisionValue = 20;
    double minYDivisionValue = -20, maxYDivisionValue = 20;

    Pane topDivisions, leftDivisions;

    TreeMap<Long, TreeMap<Float, List<SpaceMarineWithOwner>>> map;
    HashMap<String, Color> colorsForOwners;


    //Devisions
    Label topDevision0, topDevision1, topDevision2, topDevision3, topDevision4;
    Label leftDevision0, leftDevision1, leftDevision2, leftDevision3, leftDevision4;

    @FXML
    Pane leftButton, rightButton, downButton, topButton;

    public ControllerMap(Tab mapTab)
    {
        map = new TreeMap<>();

        includedSpaceMarines = new HashMap<>();

        fieldPane = (Pane) ((ScrollPane) mapTab.getContent().lookup("#scroll")).getContent().lookup("#fieldPane");

        plusButton = (Pane) mapTab.getContent().lookup("#plusButton");
        minusButton = (Pane) mapTab.getContent().lookup("#minusButton");
        topDivisions = (Pane) mapTab.getContent().lookup("#topDivisions");
        leftDivisions = (Pane) mapTab.getContent().lookup("#leftDivisions");

        leftDevision0 = (Label) mapTab.getContent().lookup("#leftDevision0");
        leftDevision1 = (Label) mapTab.getContent().lookup("#leftDevision1");
        leftDevision2 = (Label) mapTab.getContent().lookup("#leftDevision2");
        leftDevision3 = (Label) mapTab.getContent().lookup("#leftDevision3");
        leftDevision4 = (Label) mapTab.getContent().lookup("#leftDevision4");

        topDevision0 = (Label) mapTab.getContent().lookup("#topDevision0");
        topDevision1 = (Label) mapTab.getContent().lookup("#topDevision1");
        topDevision2 = (Label) mapTab.getContent().lookup("#topDevision2");
        topDevision3 = (Label) mapTab.getContent().lookup("#topDevision3");
        topDevision4 = (Label) mapTab.getContent().lookup("#topDevision4");

        leftButton = (Pane) mapTab.getContent().lookup("#leftButton");
        rightButton = (Pane) mapTab.getContent().lookup("#rightButton");
        downButton = (Pane) mapTab.getContent().lookup("#downButton");
        topButton = (Pane) mapTab.getContent().lookup("#topButton");

        Canvas canvas = new Canvas(500, 340);
        fieldPane.getChildren().add(canvas);

        leftButton.setOnMouseClicked(event -> {
            double difX = (maxXDivisionValue - minXDivisionValue) / 8.;

            topDevision0.setText(Double.toString(Double.parseDouble(topDevision0.getText()) - difX));
            topDevision1.setText(Double.toString(Double.parseDouble(topDevision1.getText()) - difX));
            topDevision2.setText(Double.toString(Double.parseDouble(topDevision2.getText()) - difX));
            topDevision3.setText(Double.toString(Double.parseDouble(topDevision3.getText()) - difX));
            topDevision4.setText(Double.toString(Double.parseDouble(topDevision4.getText()) - difX));

            maxXDivisionValue -= difX;
            minXDivisionValue -= difX;
        });

        rightButton.setOnMouseClicked(event -> {
            double difX = (maxXDivisionValue - minXDivisionValue) / 8.;

            topDevision0.setText(Double.toString(Double.parseDouble(topDevision0.getText()) + difX));
            topDevision1.setText(Double.toString(Double.parseDouble(topDevision1.getText()) + difX));
            topDevision2.setText(Double.toString(Double.parseDouble(topDevision2.getText()) + difX));
            topDevision3.setText(Double.toString(Double.parseDouble(topDevision3.getText()) + difX));
            topDevision4.setText(Double.toString(Double.parseDouble(topDevision4.getText()) + difX));

            maxXDivisionValue += difX;
            minXDivisionValue += difX;
        });

        topButton.setOnMouseClicked(event -> {
            double difY = (maxYDivisionValue - minYDivisionValue) / 8.;

            leftDevision0.setText(Double.toString(Double.parseDouble(leftDevision0.getText()) - difY));
            leftDevision1.setText(Double.toString(Double.parseDouble(leftDevision1.getText()) - difY));
            leftDevision2.setText(Double.toString(Double.parseDouble(leftDevision2.getText()) - difY));
            leftDevision3.setText(Double.toString(Double.parseDouble(leftDevision3.getText()) - difY));
            leftDevision4.setText(Double.toString(Double.parseDouble(leftDevision4.getText()) - difY));;

            maxYDivisionValue -= difY;
            minYDivisionValue -= difY;
        });

        downButton.setOnMouseClicked(event -> {
            double difY = (maxYDivisionValue - minYDivisionValue) / 8.;

            leftDevision0.setText(Double.toString(Double.parseDouble(leftDevision0.getText()) + difY));
            leftDevision1.setText(Double.toString(Double.parseDouble(leftDevision1.getText()) + difY));
            leftDevision2.setText(Double.toString(Double.parseDouble(leftDevision2.getText()) + difY));
            leftDevision3.setText(Double.toString(Double.parseDouble(leftDevision3.getText()) + difY));
            leftDevision4.setText(Double.toString(Double.parseDouble(leftDevision4.getText()) + difY));;

            maxYDivisionValue += difY;
            minYDivisionValue += difY;
        });

        plusButton.setOnMouseClicked(event -> {
            double difX = (maxXDivisionValue - minXDivisionValue) / 4, difY = (maxYDivisionValue - minYDivisionValue) / 4;

            if(difX <= 5.5 || difY <= 5.5)
                return;

            double medX = (maxXDivisionValue + minXDivisionValue) / 2, medY = (maxYDivisionValue + minYDivisionValue) / 2;
            minXDivisionValue = medX - difX;
            maxXDivisionValue = medX + difX;

            topDevision0.setText(Double.toString(minXDivisionValue));
            topDevision1.setText(Double.toString((minXDivisionValue + medX) / 2));
            topDevision2.setText(Double.toString(medX));
            topDevision3.setText(Double.toString((medX + maxXDivisionValue) / 2));
            topDevision4.setText(Double.toString(maxXDivisionValue));


            minYDivisionValue = medY - difY;
            maxYDivisionValue = medY + difY;

            leftDevision0.setText(Double.toString(minYDivisionValue));
            leftDevision1.setText(Double.toString((minYDivisionValue + medY) / 2));
            leftDevision2.setText(Double.toString(medY));
            leftDevision3.setText(Double.toString((medY + maxYDivisionValue) / 2));
            leftDevision4.setText(Double.toString(maxYDivisionValue));

        });

        minusButton.setOnMouseClicked(event -> {
            double difX = maxXDivisionValue - minXDivisionValue, difY = maxYDivisionValue - minYDivisionValue;
            double medX = (maxXDivisionValue + minXDivisionValue) / 2, medY = (maxYDivisionValue + minYDivisionValue) / 2;
            minXDivisionValue = medX - difX;
            maxXDivisionValue = medX + difX;

            topDevision0.setText(Double.toString(minXDivisionValue));
            topDevision1.setText(Double.toString((minXDivisionValue + medX) / 2));
            topDevision2.setText(Double.toString(medX));
            topDevision3.setText(Double.toString((medX + maxXDivisionValue) / 2));
            topDevision4.setText(Double.toString(maxXDivisionValue));


            minYDivisionValue = medY - difY;
            maxYDivisionValue = medY + difY;

            leftDevision0.setText(Double.toString(minYDivisionValue));
            leftDevision1.setText(Double.toString((minYDivisionValue + medY) / 2));
            leftDevision2.setText(Double.toString(medY));
            leftDevision3.setText(Double.toString((medY + maxYDivisionValue) / 2));
            leftDevision4.setText(Double.toString(maxYDivisionValue));

        });

        colorsForOwners = new HashMap<>();
    }

    public void setSpacemarinesGetter(Supplier<LinkedList<SpaceMarineWithOwner>> spacemarinesGetter)
    {
        this.spacemarinesGetter = spacemarinesGetter;
    }

    public void update(LinkedList<SpaceMarineWithOwner> spacemarines)
    {
        HashMap<SpaceMarineWithOwner, Canvas> subIncludedSpaceMarines = new HashMap<>();

        for (SpaceMarineWithOwner sm : spacemarines) {
            if(includedSpaceMarines.containsKey(sm))
            {
                subIncludedSpaceMarines.put(sm, includedSpaceMarines.get(sm));
                includedSpaceMarines.remove(sm);
                continue;
            }

            //Появление (такого объекта ещё не было)
            subIncludedSpaceMarines.put(sm, null);

            if (!map.containsKey(sm.getCoordinates().getY())) {
                TreeMap<Float, List<SpaceMarineWithOwner>> internalMap = new TreeMap<>();
                internalMap.put(sm.getCoordinates().getX(), new LinkedList<>());
                internalMap.get(sm.getCoordinates().getX()).add(sm);
                map.put(sm.getCoordinates().getY(), internalMap);
                continue;
            }


            if (!map.get(sm.getCoordinates().getY()).containsKey(sm.getCoordinates().getX())) {
                LinkedList<SpaceMarineWithOwner> internalList = new LinkedList<>();
                internalList.add(sm);
                map.get(sm.getCoordinates().getY()).put(sm.getCoordinates().getX(), internalList);
                continue;
            }

            map.get(sm.getCoordinates().getY()).get(sm.getCoordinates().getX()).add(sm);
        }

        //Удаляем тех, которые не пришли
        for(SpaceMarineWithOwner sm : includedSpaceMarines.keySet())
        {
            fieldPane.getChildren().remove(includedSpaceMarines.get(sm));

            map.get(sm.getCoordinates().getY()).get(sm.getCoordinates().getX()).remove(sm);
            if(map.get(sm.getCoordinates().getY()).get(sm.getCoordinates().getX()).isEmpty())
                map.get(sm.getCoordinates().getY()).remove(sm.getCoordinates().getX());
            if(map.get(sm.getCoordinates().getY()).isEmpty())
                map.remove(sm.getCoordinates().getY());
        }

        includedSpaceMarines = subIncludedSpaceMarines;

        drawSpaceMarines();


    }

//    private void internalUpdating()
//    {
//        drawSpaceMarines();
//    }

    private void drawSpaceMarines()
    {
        //fieldPane.getChildren().clear();

        Long minKey = map.ceilingKey(Math.round(Math.floor(minYDivisionValue-340./4.)));

        HashMap<SpaceMarineWithOwner, Canvas> subIncludedSpaceMarines = new HashMap<>();

        while (minKey != null) {

            if (minKey > maxYDivisionValue + 340./4.)
                break;
            Float floatMinKey = map.get(minKey).ceilingKey((float) minXDivisionValue - 500.f/8.f);

            while (floatMinKey != null) {

                if (floatMinKey > maxXDivisionValue + 500.f/8.f)
                    break;

                for (SpaceMarineWithOwner sm : map.get(minKey).get(floatMinKey)) {

                    double width = 500. / 10. * 40. / (maxXDivisionValue - minXDivisionValue), height = width / 3. * 8.;

                    double xOffsetSm = sm.getCoordinates().getX() - minXDivisionValue;
                    double yOffsetSm = sm.getCoordinates().getY() - minYDivisionValue;

                    Canvas oldCanvas = includedSpaceMarines.get(sm);

                    if (includedSpaceMarines.get(sm) != null)
                    {
                        //Если уже нарисован точно такой же -- пропускаем
                        if (Math.abs(oldCanvas.getWidth() - width * sm.getHealth() / 100.) <= 0.000001 && Math.abs(oldCanvas.getLayoutX() - (xOffsetSm * 500. / (maxXDivisionValue - minXDivisionValue) - width / 2.)) <= 0.000001 && Math.abs(oldCanvas.getLayoutY() - (yOffsetSm * 340. / (maxYDivisionValue - minYDivisionValue) - height / 2.)) <= 0.000001)
                        {
                            subIncludedSpaceMarines.put(sm, oldCanvas);
                            includedSpaceMarines.remove(sm);
                            continue;
                        }
                    }

                    Canvas canvas = new Canvas(width * sm.getHealth() / 100., height * sm.getHealth() / 100.);

                    canvas.setLayoutX(xOffsetSm * 500. / (maxXDivisionValue - minXDivisionValue) - width / 2.);
                    canvas.setLayoutY(yOffsetSm * 340. / (maxYDivisionValue - minYDivisionValue) - height / 2.);

                    subIncludedSpaceMarines.put(sm, canvas);
                    includedSpaceMarines.remove(sm);


                    canvas.setOnMouseClicked(event -> {
                        //TODO подсказка
                        System.out.println(sm.getId());
                    });

                    fieldPane.getChildren().add(canvas);
                    fieldPane.getChildren().remove(oldCanvas);

                    if(!colorsForOwners.containsKey(sm.getOwner()))
                        colorsForOwners.put(sm.getOwner(), Color.rgb((Math.abs((new Random()).nextInt()))%256, Math.abs((new Random()).nextInt())%256, Math.abs((new Random()).nextInt())%256));


                    drawSpaceMarine(sm, canvas.getGraphicsContext2D(), colorsForOwners.get(sm.getOwner()));
                }

                floatMinKey = map.get(minKey).higherKey(floatMinKey);
            }

            minKey = map.higherKey(minKey);
        }

        for(SpaceMarineWithOwner sm : includedSpaceMarines.keySet())
            fieldPane.getChildren().remove(includedSpaceMarines.get(sm));

        includedSpaceMarines = subIncludedSpaceMarines;
    }

    public void drawSpaceMarine(SpaceMarineWithOwner sm, GraphicsContext gc, Color color)
    {
        double width = gc.getCanvas().getWidth(), height = gc.getCanvas().getHeight();

        gc.setStroke(Paint.valueOf(color.toString()));

        gc.setLineWidth(2);

        gc.strokeRoundRect(width/4, height/7, width/2, height/6, width/4, width/6);

        gc.strokeArc(2 + (width-4)/2- (width - 4)/1.5/2,2, (width - 4)/1.5, height/3. - 2, 0, 180, ArcType.ROUND);

        gc.setFill(Paint.valueOf(color.toString()));
        gc.fillArc(2 + (width-4)/2 - (width - 4)/1.5/2,2, (width - 4)/1.5, height/3. - 2, 0, 180, ArcType.ROUND);

        gc.strokeOval(width/2*7/10, height/5.5, width/10, width/10);
        gc.strokeOval(width - width/2*9/10, height/5.5, width/10, width/10);

        gc.strokeLine(width/2*8/10, height/3.8, width-width/2*8/10, height/3.8);

        gc.strokeLine(width/2*8/10, height/7 + height/6, width/2*8/10, height/7 + height/6 + height/25);
        gc.strokeLine(width - width/2*8/10, height/7 + height/6, width - width/2*8/10, height/7 + height/6 + height/25);


        gc.beginPath();
        gc.moveTo(width/2*8/10, height/7+height/6+height/25);
        gc.bezierCurveTo(width/4*5/10, height/2*7/10, width/4*1/10, height/2*7/10, width/4*3/10, height/2 + height/8);
        gc.stroke();
        gc.closePath();

        gc.beginPath();
        gc.moveTo(width-width/2*8/10, height/7+height/6+height/25);
        gc.bezierCurveTo(width - width/4*5/10, height/2*7/10, width - width/4*1/10, height/2*7/10, width - width/4*3/10, height/2 + height/8);
        gc.stroke();
        gc.closePath();


        gc.strokeLine(width/4 + width/20, height/2.2, width/4 + width/15, height/1.6);
        gc.strokeLine(width - (width/4 + width/20), height/2.2, width - (width/4 + width/15), height/1.6);


        gc.strokeLine(width/4 + width/15, height/1.6, width - (width/4 + width/15), height/1.6);


        gc.strokeLine(width/4 + width/15, height/1.6, width/4, height - height/20);
        gc.strokeLine(width - (width/4 + width/15), height/1.6, width - width/4, height - height/20);

        gc.strokeLine(width/2, height/1.4, width/4 + width/6, height - height/20);
        gc.strokeLine(width/2, height/1.4, width - (width/4 + width/6), height - height/20);

        gc.strokeLine(width/4, height - height/20, width/4 + width/6, height - height/20);
        gc.strokeLine(width - width/4, height - height/20, width - (width/4 + width/6), height - height/20);

    }
}
