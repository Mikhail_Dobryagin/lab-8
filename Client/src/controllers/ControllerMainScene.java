package controllers;

import com.company.Command_packed;
import database.User;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import spacemarine.SpaceMarineWithOwner;
import util.CommandExecutor;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Function;
import java.util.function.Supplier;


public class ControllerMainScene
{
    @FXML
    Pane closeButton;
    @FXML
    Pane collapseButton;
    @FXML
    Label userLabel;
    @FXML
    TabPane tabPane;
    @FXML
    Pane mainPane;
    @FXML
    Pane movePane;

    ControllerCommands controllerCommands;
    ControllerTable controllerTable;
    ControllerMap controllerMap;

    Supplier<LinkedList<SpaceMarineWithOwner>> spacemarinesGetter;
    LinkedList<SpaceMarineWithOwner> spaceMarineWithOwners;


    double xOffset = 0, yOffset = 0;

    public ControllerMainScene()
    {

    }

    public void init(User user)
    {
        userLabel.setText(user.getLogin());
        closeButton.setOnMouseClicked(event -> ((Stage) ((Node) event.getSource()).getScene().getWindow()).close());
        collapseButton.setOnMouseClicked(event -> ((Stage)((Node) event.getSource()).getScene().getWindow()).setIconified(true));


        movePane.setOnMousePressed(event -> {
            Stage primaryStage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
            xOffset = primaryStage.getX() - event.getScreenX();
            yOffset = primaryStage.getY() - event.getScreenY();
        });
        movePane.setOnMouseDragged(event -> {
            Stage primaryStage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
            primaryStage.setX(event.getScreenX() + xOffset);
            primaryStage.setY(event.getScreenY() + yOffset);
        });

        controllerCommands = new ControllerCommands(tabPane.getTabs().get(0));

        controllerTable = new ControllerTable(tabPane.getTabs().get(1));

        controllerMap = new ControllerMap(tabPane.getTabs().get(2));

        Timer timer = new Timer();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                Platform.runLater(() -> {
                    if (!tabPane.getScene().getWindow().isShowing()) {
                        System.out.println("Всё");
                        System.exit(0);
                    }

                    updateSpaceMarines();
                });
            }
        }, 0, 50);  //Если уменьшать, могут начать вылетать исключения (с этим, вроде, разобрался)

        spaceMarineWithOwners = new LinkedList<>();

    }

    public void addCommandGetter(Function<String, Command_packed> commandGetter)
    {
        controllerCommands.setCommandGetter(commandGetter);
    }

    public void registerCommandExecutor(CommandExecutor commandExecutor)
    {
        controllerCommands.setCommandExecutor(commandExecutor);
    }

    public void setSpacemarinesGetter(Supplier spacemarinesGetter)
    {
        this.spacemarinesGetter = spacemarinesGetter;
    }

    private void updateSpaceMarines()
    {
        this.spaceMarineWithOwners = this.spacemarinesGetter.get();
        this.controllerTable.update(spaceMarineWithOwners);
        this.controllerMap.update(spaceMarineWithOwners);
    }

    /*
    public void pushing()
    {
        tabPane.getSelectionModel().select((tabPane.getSelectionModel().getSelectedIndex()+1)%3);
    }
    */

    public ControllerCommands getCommandsController()
    {
        return this.controllerCommands;
    }
}
