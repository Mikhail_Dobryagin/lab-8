package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import util.Attempt;
import util.ErrorReturn;


public class ControllerLogIn
{
    @FXML
    Button loginButton;
    @FXML
    Button registerButton;
    @FXML
    TextField loginField;
    @FXML
    PasswordField password1;
    @FXML
    PasswordField password2;
    @FXML
    Label warningField;

    Attempt<String> loginAction, registerAction;

    public ControllerLogIn()
    {

    }

    public void registerButtonClicked()
    {
        if (registerButton.getText().equals("Регистрация")) {
            registerButton.setText("Зарегистрироваться");
            password2.setVisible(true);
            return;
        }

        if (!password1.getText().equals(password2.getText())) {
            warningField.setText("Пароли не совпадают");
            warningField.setVisible(true);
            return;
        }

        registerButton.setDisable(true);
        loginButton.setDisable(true);

        ErrorReturn errorReturn = registerAction.tryAction(loginField.getText(), password1.getText());
        if (errorReturn.getCode() == 0)
            return;

        registerButton.setText("Регистрация");
        warningField.setText(errorReturn.getStatus());
        warningField.setVisible(true);
        registerButton.setDisable(false);
        loginButton.setDisable(false);
    }

    public void loginButtonClicked()
    {
        registerButton.setDisable(true);
        loginButton.setDisable(true);

        ErrorReturn errorReturn = loginAction.tryAction(loginField.getText(), password1.getText());
        if (errorReturn.getCode() == 0)
            return;

        warningField.setText(errorReturn.getStatus());
        warningField.setVisible(true);
        registerButton.setDisable(false);
        loginButton.setDisable(false);
    }

    public void setError(String message)
    {
        warningField.setText(message);
        warningField.setVisible(true);
    }

    public void setLoginAction(Attempt<String> loginAction)
    {
        this.loginAction = loginAction;
    }

    public void setRegisterAction(Attempt<String> registerAction)
    {
        this.registerAction = registerAction;
    }

}
