package controllers;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import util.ErrorReturn;

public class ControllerError
{
    double xOffset = 0, yOffset = 0;

    @FXML
    TextArea message;

    @FXML
    Pane closeButton;
    @FXML
    Pane collapseButton;
    @FXML
    Pane movePane;


    public void setError(ErrorReturn error)
    {
        message.setText(error.getStatus());
    }

    public void init()
    {
        closeButton.setOnMouseClicked(event -> ((Stage) ((Node) event.getSource()).getScene().getWindow()).close());
        collapseButton.setOnMouseClicked(event -> ((Stage)((Node) event.getSource()).getScene().getWindow()).setIconified(true));


        movePane.setOnMousePressed(event -> {
            Stage primaryStage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
            xOffset = primaryStage.getX() - event.getScreenX();
            yOffset = primaryStage.getY() - event.getScreenY();
        });
        movePane.setOnMouseDragged(event -> {
            Stage primaryStage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
            primaryStage.setX(event.getScreenX() + xOffset);
            primaryStage.setY(event.getScreenY() + yOffset);
        });
    }
}
