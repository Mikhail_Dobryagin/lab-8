package commands.client;

import com.company.Command;
import network.PacketCtoS;
import util.ErrorReturn;

import java.util.LinkedList;

public class CommandClear implements Command
{
    @Override
    public ErrorReturn execute(PacketCtoS outputPacket)
    {
        return ErrorReturn.OK();
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs, PacketCtoS outputPacket)
    {
        return null;
    }

}
