package commands.server;

import com.company.CommandProcessingServerReturn;
import network.PacketStoC;


public class CommandSout implements CommandProcessingServerReturn
{
    @Override
    public String execute(PacketStoC answerPacket)
    {
        if (answerPacket.getString() != null)
            return answerPacket.getString();
        return "";
    }
}
