package commands.server;

import com.company.CommandProcessingServerReturn;
import network.PacketStoC;
import util.ErrorReturn;


public class CommandShow implements CommandProcessingServerReturn
{

    public static StringBuilder longLine()
    {
        return new StringBuilder("________________________________________________________________________________________________________________________").append("\n");
    }

    @Override
    public String execute(PacketStoC answerPacket)
    {
        ErrorReturn errorReturn = answerPacket.getErrorReturn();

        if (errorReturn.getCode() != 0)
            return errorReturn.getStatus();

        StringBuilder result = new StringBuilder();

        result.append("\n").append(longLine());

        while (!answerPacket.getSpaceMarines().isEmpty())
            result.append(answerPacket.getSpaceMarines().poll().show()).append("\n");

        result.append(longLine()).append("\n");

        return result.toString();
    }
}
