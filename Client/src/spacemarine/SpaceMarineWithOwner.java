package spacemarine;

import java.time.LocalDate;

public class SpaceMarineWithOwner extends SpaceMarine
{
    String owner;

    public static final long serialVersionUID = 12L;

    public SpaceMarineWithOwner(Long id, String name, Coordinates coordinates, double health, AstartesCategory category, Weapon weaponType, MeleeWeapon meleeWeapon, Chapter chapter)
    {
        super(id, name, coordinates, health, category, weaponType, meleeWeapon, chapter);
    }

    public SpaceMarineWithOwner(Long id, String name, Coordinates coordinates, double health, AstartesCategory category, Weapon weaponType, MeleeWeapon meleeWeapon, Chapter chapter, String owner)
    {
        super(id, name, coordinates, health, category, weaponType, meleeWeapon, chapter);
        this.owner = owner;
    }

    public SpaceMarineWithOwner(Long id, String name, Coordinates coordinates, LocalDate localDate, double health, AstartesCategory category, Weapon weaponType, MeleeWeapon meleeWeapon, Chapter chapter)
    {
        super(id, name, coordinates, localDate, health, category, weaponType, meleeWeapon, chapter);
    }

    public SpaceMarineWithOwner(Long id, String name, Coordinates coordinates, LocalDate localDate, double health, AstartesCategory category, Weapon weaponType, MeleeWeapon meleeWeapon, Chapter chapter, String owner)
    {
        super(id, name, coordinates, localDate, health, category, weaponType, meleeWeapon, chapter);
        this.owner = owner;
    }


    public SpaceMarineWithOwner(SpaceMarine spaceMarine)
    {
        super(spaceMarine.getId(), spaceMarine.getName(), spaceMarine.getCoordinates(), spaceMarine.getCreationDate(), spaceMarine.getHealth(), spaceMarine.getCategory(), spaceMarine.getWeaponType(), spaceMarine.getMeleeWeapon(), spaceMarine.getChapter());
    }

    public SpaceMarineWithOwner(SpaceMarine spaceMarine, String owner)
    {
        this(spaceMarine);
        this.owner = owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getOwner()
    {
        return this.owner;
    }

    @Override
    public int hashCode()
    {
        return super.hashCode() + owner.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null || obj.getClass() != this.getClass())
            return false;
        if(obj == this)
            return true;

        SpaceMarineWithOwner sm = (SpaceMarineWithOwner) obj;

        return super.equals(obj) && owner.equals(sm.owner);
    }
}
