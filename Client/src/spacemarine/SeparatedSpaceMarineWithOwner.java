package spacemarine;

import java.time.LocalDate;

public class SeparatedSpaceMarineWithOwner
{
    private Long id, y;
    private double health;
    private float x;
    private String name, chapterName, parentLegion, owner;
    private LocalDate creationDate;
    private AstartesCategory category;
    private Weapon weaponType;
    private MeleeWeapon meleeWeapon;

    public SeparatedSpaceMarineWithOwner(SpaceMarineWithOwner spaceMarine)
    {
        id = spaceMarine.getId();
        health = spaceMarine.getHealth();
        x = spaceMarine.getCoordinates().getX();
        y = spaceMarine.getCoordinates().getY();
        creationDate = spaceMarine.getCreationDate();
        name = spaceMarine.getName();
        chapterName = spaceMarine.getChapter() == null ? "" : spaceMarine.getChapter().getName();
        parentLegion = spaceMarine.getChapter() == null ? "" : spaceMarine.getChapter().getParentLegion();
        category = spaceMarine.getCategory();
        weaponType = spaceMarine.getWeaponType();
        meleeWeapon = spaceMarine.getMeleeWeapon();
        owner = spaceMarine.getOwner();
    }

    public Long getId()
    {
        return id;
    }

    public Long getY()
    {
        return y;
    }

    public double getHealth()
    {
        return health;
    }

    public float getX()
    {
        return x;
    }

    public String getName()
    {
        return name;
    }

    public String getChapterName()
    {
        return chapterName;
    }

    public String getParentLegion()
    {
        return parentLegion;
    }

    public String getOwner()
    {
        return owner;
    }

    public LocalDate getCreationDate()
    {
        return creationDate;
    }

    public AstartesCategory getCategory()
    {
        return category;
    }

    public Weapon getWeaponType()
    {
        return weaponType;
    }

    public MeleeWeapon getMeleeWeapon()
    {
        return meleeWeapon;
    }
}
