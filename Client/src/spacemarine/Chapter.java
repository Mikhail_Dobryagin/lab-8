package spacemarine;

import java.io.Serializable;

/**
 * Класс, обозначающий командира и подразделение десатника
 */
public class Chapter implements Serializable
{
    public static final long serialVersionUID = 2L;
    public static class MakeChapterException extends RuntimeException
    {
        public MakeChapterException(String message)
        {
            super(message);
        }
    }
    /**
     * @value Имя командира
     */
    private String chapterName; //Поле не может быть null, Строка не может быть пустой
    /**
     * @value Название подразделения
     */
    private String parentLegion;

    //Getters and setters
    /** * @exclude */
    public String getName() {
        return chapterName;
    }

    /** * @exclude */
    public void setName(String name) {
        if(!isNameCorrect(name))
            throw new MakeChapterException("Имя командира должно быть введено без странных символов");
        this.chapterName = name;
    }
    /** * @exclude */
    public String getParentLegion() {
        return parentLegion;
    }
    /** * @exclude */
    public void setParentLegion(String parentLegion) {
        if(!isPLCorrect(parentLegion))
            throw new MakeChapterException("Должно быть введено название поодразделения без странных символов");
        this.parentLegion = parseToPL(parentLegion);
    }
    //!Getters and setters

    public static boolean isNameCorrect(String name)
    {
        return !name.matches(".*[<>\\n\\t].*") && name.matches(".*[\\wА-Яа-яёЁ\\d].*");
    }

    public static boolean isPLCorrect(String parentLegion)
    {
        return !parentLegion.matches(".*[<>].*");
    }

    public static String parseToPL(String s)
    {
        if (!isPLCorrect(s))
            throw new MakeChapterException("Название подразделения введено неверно");

        StringBuilder parentLegion= new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) > 23)
                parentLegion.append(s.charAt(i));
        }

        return parentLegion.toString();
    }

    /**
     * Конструктор
     * @param name Имя командира
     * @param parentLegion Название подразделения
     */
    public Chapter(String name, String parentLegion) {
        setName(name);
        setParentLegion(parseToPL(parentLegion));
    }

    public static Chapter newChapter(String sign, String name, String parentLegion)
    {
        if(sign.equals("-"))
            return null;
        if(!sign.equals("+"))
            throw  new MakeChapterException("Не указано, есть ли глава");
        return new Chapter(name, parentLegion);
    }

    public Chapter()
    {

    }

    public static StringBuilder show(Chapter chapter)
    {
        if(chapter==null)
            return new StringBuilder();

        StringBuilder stringOutput = new StringBuilder();

        stringOutput.append("\tКомандир               | ").append(chapter.getName()).append("\n");

        if(chapter.getParentLegion().isEmpty())
            return stringOutput;

        stringOutput.append("\tПодразделение          | ").append(chapter.getParentLegion()).append("\n");

        return stringOutput;
    }

    @Override
    public int hashCode()
    {
        return this.getName().hashCode() + this.getParentLegion().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null || obj.getClass() != this.getClass())
            return false;
        if(obj == this)
            return true;

        Chapter chapter = (Chapter) obj;

        return chapterName.equals(chapter.getName()) && parentLegion.equals(chapter.getParentLegion());
    }
}
