package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import database.StudsDbConnection;
import database.User;
import network.PacketCtoS;
import network.PacketStoC;
import spacemarine.SpaceMarine;
import spacemarine.SpaceMarineWithOwner;

import java.sql.SQLException;

public class CommandUpdate implements Command
{
    private final PQofSpacemarines pqs;
    private final StudsDbConnection db;

    public CommandUpdate(StudsDbConnection db, PQofSpacemarines pqs)
    {
        this.pqs = pqs;
        this.db = db;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {

        User user = inputPacket.getUser();

        Long id = inputPacket.getId();
        SpaceMarineWithOwner newSm = new SpaceMarineWithOwner(inputPacket.getSpaceMarine(), user.getLogin());

        PacketStoC outputPacket = new PacketStoC();

        pqs.lock.writeLock().lock();
        try {
            if (db.updateSm(user, id, newSm))
                outputPacket.setString(pqs.update(id, newSm) ? "Элемент успешно обновлён" : "Элемент с таким id не найден");
            else
                outputPacket.setString("Не удалось обновить элемент");
        } catch (SQLException e) {
            outputPacket.setString(e.getMessage());
        } finally {
            pqs.lock.writeLock().unlock();
        }


        return outputPacket;
    }
}
