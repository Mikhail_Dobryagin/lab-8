package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import database.StudsDbConnection;
import database.User;
import network.PacketCtoS;
import network.PacketStoC;
import spacemarine.SpaceMarine;
import spacemarine.SpaceMarineWithOwner;
import util.ErrorReturn;

import java.sql.SQLException;
import java.util.LinkedList;

public class CommandRemoveHead implements Command
{
    private final PQofSpacemarines pqs;
    private final StudsDbConnection db;

    public CommandRemoveHead(StudsDbConnection db, PQofSpacemarines pqs)
    {
        this.pqs = pqs;
        this.db = db;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        User user = inputPacket.getUser();

        pqs.lock.writeLock().lock();
        try {
            SpaceMarine head = pqs.getHead();
            if (head == null) {
                outputPacket.setErrorReturn(new ErrorReturn(2, "Очередь пуста"));
                return outputPacket;
            }

            try {
                if (!db.removeSmById(user, head.getId())) {
                    outputPacket.setErrorReturn(new ErrorReturn(2, "Не удалось удалить элемент"));
                    return outputPacket;
                }

                SpaceMarineWithOwner sm = pqs.remove_head();
                outputPacket.setErrorReturn(ErrorReturn.OK());
                outputPacket.setSpaceMarines(new LinkedList<>());

                if (sm != null)
                    outputPacket.getSpaceMarines().add(sm);

            } catch (SQLException e) {
                outputPacket.setErrorReturn(new ErrorReturn(1, e.getMessage()));
            }
        } finally {
            pqs.lock.writeLock().unlock();
        }
        return outputPacket;
    }


}
