package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import database.StudsDbConnection;
import database.User;
import network.PacketCtoS;
import network.PacketStoC;

import java.sql.SQLException;

public class CommandRemoveById implements Command
{
    private final PQofSpacemarines pqs;
    private final StudsDbConnection db;

    public CommandRemoveById(StudsDbConnection db, PQofSpacemarines pqs)
    {
        this.pqs = pqs;
        this.db = db;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        User user = inputPacket.getUser();

        pqs.lock.writeLock().lock();

        try {
            Long id = inputPacket.getId();

            if (db.removeSmById(user, id))
                outputPacket.setString(pqs.remove_by_id(id) ? "Элемент успешно удалён" : "Элемента с таким id не существует");
            else
                outputPacket.setString("Не удалось удалить элемент");
        } catch (NumberFormatException e) {
            outputPacket.setString("Ошибка при вводе Id");
        } catch (SQLException e) {
            outputPacket.setString(e.getMessage());
        } finally {
            pqs.lock.writeLock().unlock();
        }

        return outputPacket;
    }
}
