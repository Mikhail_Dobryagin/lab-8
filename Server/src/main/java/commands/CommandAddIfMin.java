package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import database.StudsDbConnection;
import database.User;
import network.PacketCtoS;
import network.PacketStoC;
import spacemarine.Chapter;
import spacemarine.SpaceMarine;
import spacemarine.SpaceMarineWithOwner;

import java.sql.SQLException;

public class CommandAddIfMin implements Command
{
    private final PQofSpacemarines pqs;
    private final StudsDbConnection db;

    public CommandAddIfMin(StudsDbConnection db, PQofSpacemarines pqs)
    {
        this.pqs = pqs;
        this.db = db;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        User user = inputPacket.getUser();

        SpaceMarineWithOwner sm = new SpaceMarineWithOwner(inputPacket.getSpaceMarine(), user.getLogin());

        pqs.lock.writeLock().lock();

        try {

            if (!pqs.is_min(sm)) {
                outputPacket.setString("Данный элемент не минимальный");
                return outputPacket;
            }

            try {
                if (db.insertSm(user, sm))
                    outputPacket.setString(pqs.add(sm) ? "Вставка прошла успешно" : "Не удалось вставить элемент");
                else
                    outputPacket.setString("Не удалось вставить элемент");
            } catch (IllegalArgumentException | Chapter.MakeChapterException | SpaceMarine.MakeSpacemarineException | SQLException e) {
                outputPacket.setString(e.getMessage());
                return outputPacket;
            }

        } finally {
            pqs.lock.writeLock().unlock();
        }

        return outputPacket;
    }

}
