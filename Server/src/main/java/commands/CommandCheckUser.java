package commands;

import com.company.Command;
import database.CheckUserResult;
import database.StudsDbConnection;
import network.PacketCtoS;
import network.PacketStoC;
import util.ErrorReturn;

import java.sql.SQLException;

public class CommandCheckUser implements Command
{
    StudsDbConnection db;

    public CommandCheckUser(StudsDbConnection db)
    {
        this.db = db;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        try {
            CheckUserResult checkUser = db.checkUser(inputPacket.getUser());
            outputPacket.setErrorReturn(ErrorReturn.OK());
            outputPacket.setResult(checkUser);
        } catch (SQLException e) {
            outputPacket.setErrorReturn(new ErrorReturn(2, e.getMessage()));
        }

        return outputPacket;
    }
}
