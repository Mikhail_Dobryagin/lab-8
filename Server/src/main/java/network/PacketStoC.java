package network;

import database.CheckUserResult;
import spacemarine.SpaceMarineWithOwner;
import util.ErrorReturn;

import java.io.Serializable;
import java.util.LinkedList;

public class PacketStoC implements Serializable
{
    public static final long serialVersionUID = 12L;

    private ErrorReturn errorReturn = ErrorReturn.OK();
    private LinkedList<SpaceMarineWithOwner> spaceMarines = null;
    private CheckUserResult result;
    private String string;

    public ErrorReturn getErrorReturn()
    {
        return errorReturn;
    }

    public void setErrorReturn(ErrorReturn errorReturn)
    {
        this.errorReturn = errorReturn;
    }

    public LinkedList<SpaceMarineWithOwner> getSpaceMarines()
    {
        return spaceMarines;
    }

    public void setSpaceMarines(LinkedList<SpaceMarineWithOwner> spaceMarines)
    {
        this.spaceMarines = spaceMarines;
    }

    public CheckUserResult getResult()
    {
        return result;
    }

    public void setResult(CheckUserResult result)
    {
        this.result = result;
    }

    public String getString()
    {
        return string;
    }

    public void setString(String string)
    {
        this.string = string;
    }
}
