package com.company;


import network.PacketCtoS;
import network.PacketStoC;

/**
 * Интерфейс обобщающий различные команды
 */
public interface Command
{
    PacketStoC execute(PacketCtoS args);

}
