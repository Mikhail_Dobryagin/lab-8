package com.company;

import database.StudsDbConnection;
import spacemarine.SpaceMarine;
import spacemarine.SpaceMarineWithOwner;
import util.ErrorReturn;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Очередь с приоритетом, состоящая из космических десантников, приоритет которой определяется по <b>уровню здоровья</b> десатников
 */
public class PQofSpacemarines
{
    private final HashSet<Long> set = new HashSet<>();

    public ReadWriteLock lock = new ReentrantReadWriteLock();

    /**
     * @value Дата создания структура
     */
    private LocalDate creationDate;
    /**
     * @value Элементы коллекции
     */
    private PriorityQueue<SpaceMarineWithOwner> pq;

    public PQofSpacemarines()
    {
        pq = new PriorityQueue<>();
        creationDate = LocalDate.now();
    }

    /**
     * @exclude
     */
    public LocalDate getCreationDate()
    {
        return creationDate;
    }

    /**
     * @exclude
     */
    public void setCreationDate(LocalDate creationDate)
    {
        this.creationDate = creationDate;
    }

    /**
     * @exclude
     */
    public PriorityQueue<SpaceMarineWithOwner> getPq()
    {
        return pq;
    }

    /**
     * @exclude
     */
    public void setPq(PriorityQueue<SpaceMarineWithOwner> pq)
    {
        this.pq = pq;
    }

    /**
     * Вывести справку по доступным командам
     */
    public static StringBuilder help()
    {
        StringBuilder stringOutput = new StringBuilder();

        stringOutput.append("\n").append("\n");
        stringOutput.append(longLine()).append("\n");
        stringOutput.append("СПРАВКА ПО КОМАНДАМ").append("\n");
        stringOutput.append("____________________").append("\n");
        stringOutput.append("\nhelp : вывести справку по доступным командам").append("\n");
        stringOutput.append("\ninfo : вывести информацию об отряде десантников (тип, дата создания отряда, количество десантников и т.д.)").append("\n");
        stringOutput.append("\nshow : вывести всех десантников в строковом представлении").append("\n");
        stringOutput.append("\nadd : добавить нового десантника").append("\n");
        stringOutput.append("\tПомимо этого будет предложено ввести следующие поля:").append("\n");
        stringOutput.append("\t•Имя\n\t•Координаты\n\t•Здоровье\n\t•Категория\n\t•Оружие\n\t•Вспомогательное оружие\n\t•Имя командира и название отряда").append("\n");
        stringOutput.append("\nupdate <id> : обновить десантника, id которой равен заданному").append("\n");
        stringOutput.append("\tПомимо этого будет предложено ввести следующие поля:").append("\n");
        stringOutput.append("\t•Имя\n\t•Координаты\n\t•Здоровье\n\t•Категория\n\t•Оружие\n\t•Вспомогательное оружие\n\t•Имя командира и название отряда").append("\n");
        stringOutput.append("\nremove_by_id <id> : удалить десантника по его id").append("\n");
        stringOutput.append("\nclear : очистить отряд десантников").append("\n");
        stringOutput.append("\nexecute_script <имя файла> : считать и исполнить набор команд из указанного файла.\nВ нём содержатся команды в таком же виде, в котором их их можно ввести в интерактивном режиме.").append("\n");
        stringOutput.append("\nexit : завершить программу (без сохранения в файл)").append("\n");
        stringOutput.append("\nremove_first : удалить первого десантника в отряде").append("\n");
        stringOutput.append("\nremove_head : вывести первого десантника и удалить его").append("\n");
        stringOutput.append("\nadd_if_min : добавить нового десантника в отряд, если его значение меньше, чем у наименьшего десантника в отряде").append("\n");
        stringOutput.append("\tПомимо этого будет предложено ввести следующие поля:").append("\n");
        stringOutput.append("\t•Имя\n\t•Координаты\n\t•Здоровье\n\t•Категория\n\t•Оружие\n\t•Вспомогательное оружие\n\t•Имя командира и название отряда").append("\n");
        stringOutput.append("\nfilter_contains_name <подстрока> : вывести десантников, имена которых содержат заданную подстроку").append("\n");
        stringOutput.append("\nfilter_starts_with_name <префикс> : вывести десантников, имена которых начинаются с заданной подстроки").append("\n");
        stringOutput.append("\nprint_descending : вывести всех десантников отряда в порядке убывания").append("\n");
        stringOutput.append(longLine()).append("\n");
        stringOutput.append("\n").append("\n");

        return stringOutput;
    }

    /**
     * Вывести информацию о коллекции
     */
    public StringBuilder info()
    {
        StringBuilder stringOutput = new StringBuilder();

        stringOutput.append("\n");
        stringOutput.append(longLine());
        stringOutput.append("Тип коллекции:        очередь с приоритетом").append("\n");
        stringOutput.append("Элементы коллекции:   служащие космического десанта").append("\n");
        stringOutput.append("Приоритет:            здоровье десантника").append("\n");
        stringOutput.append("Дата инициализации:   ").append(creationDate).append("\n");
        stringOutput.append("Количество элементов: ").append(pq.size()).append("\n");
        stringOutput.append(longLine());
        stringOutput.append("\n");

        return stringOutput;
    }

    public static StringBuilder longLine()
    {
        return new StringBuilder("________________________________________________________________________________________________________________________").append("\n");
    }

    /**
     * Вывести все элементы структуры
     */
    public LinkedList<SpaceMarineWithOwner> show()
    {
        return new LinkedList<>(pq);
    }

    /**
     * Добавить элемент в коллекцию
     */
    public boolean add(SpaceMarineWithOwner o)
    {
        if (o.getId() <= 0 || set.contains(o.getId()))
            throw new IllegalArgumentException("Неправильно введён id");

        pq.add(o);
        set.add(o.getId());
        return true;
    }

    /**
     * Удалить элемент из структуры по его Id
     *
     * @return True -- если элемент с таким Id существует
     */
    public boolean remove_by_id(Long id)
    {
        return pq.removeIf(spaceMarine -> spaceMarine.getId().equals(id));
    }

    /**
     * Очистить структуру
     */
    public void clear()
    {
        pq.clear();
    }

    /**
     * Обновить элемент в структуре по его Id
     *
     * @param id    Id обновляемого элемента
     * @param newSm Элемент, замещающий элемент по указанному Id
     * @return True -- если существует элемент с данным Id
     */
    public boolean update(Long id, SpaceMarineWithOwner newSm)
    {
        LocalDate date = null;

        PriorityQueue<SpaceMarineWithOwner> newPq = new PriorityQueue<>();


        while (pq.size() != 0) {
            SpaceMarineWithOwner curSm = pq.poll();
            if (date == null && curSm.getId().equals(id)) {
                date = curSm.getCreationDate();
                curSm = newSm;
                curSm.setId(id);
                curSm.setCreationDate(date);
            }
            newPq.add(curSm);
        }
        pq = newPq;

        return date != null;
    }

    /**
     * Удалить первый элемент из коллекции
     *
     * @return True -- если коллекция не пуста
     */
    public boolean remove_first()
    {
        if (pq.size() > 0) {
            pq.poll();
            return true;
        }

        return false;
    }

    /**
     * вывести первый элемент коллекции и удалить его
     *
     * @return True -- если структура не пуста
     */
    public SpaceMarineWithOwner remove_head()
    {
        if (pq.isEmpty())
            return null;

        return pq.poll();
    }

    public SpaceMarineWithOwner getHead()
    {
        return pq.isEmpty() ? null : pq.peek();
    }

    /**
     * добавить новый элемент в коллекцию, если его значение меньше, чем у наименьшего элемента этой коллекции
     *
     * @return True -- если удалось добавить элемент
     */
    public boolean is_min(SpaceMarine newSm)
    {
        for (SpaceMarine curSm : pq) {
            if (newSm.compareTo(curSm) > 0)
                return false;
            if (newSm.compareTo(curSm) < 0)
                continue;

            if (newSm.getCoordinates().compareTo(curSm.getCoordinates()) > 0)
                return false;
            if (newSm.getCoordinates().compareTo(curSm.getCoordinates()) < 0)
                continue;

            return false;
        }

        return true;
    }

    /**
     * Вывести элементы, значение поля name которых содержит заданную подстроку
     */
    public LinkedList<SpaceMarineWithOwner> filter_contains_name(String s)
    {
        LinkedList<SpaceMarineWithOwner> result = new LinkedList<>();

        pq.stream().filter(sm -> sm.getName().contains(s)).forEach(result::add);

        return result;
    }

    /**
     * Вывести элементы, значение поля name которых начинается с заданной подстроки
     */
    public LinkedList<SpaceMarineWithOwner> filter_starts_with_name(String s)
    {
        LinkedList<SpaceMarineWithOwner> outputArgs = new LinkedList<>();

        pq.stream().filter(sm -> sm.getName().startsWith(s)).forEach(outputArgs::add);
        return outputArgs;
    }

    /**
     * Вывести элементы коллекции в порядке убывания
     */
    public LinkedList<SpaceMarineWithOwner> print_descending()
    {
        LinkedList<SpaceMarineWithOwner> outputArgs = new LinkedList<>();

        pq.stream().sorted((o1, o2) -> {
            int result = o1.compareTo(o2);

            if (result != 0)
                return -result;

            result = o1.getCoordinates().compareTo(o2.getCoordinates());
            if (result != 0)
                return -result;

            return -o1.getId().compareTo(o2.getId());
        }).forEach(outputArgs::add);

        return outputArgs;
    }


    private static void getSmFromResultSet(ResultSet resultSet) throws SQLException
    {

        while (resultSet.next()) {
            Long id = resultSet.getLong("id");
        }
    }

    public ErrorReturn addFromDB(StudsDbConnection connection)
    {
        try {
            pq = connection.getSpaceMarines();
            creationDate = connection.getCreationDate().toLocalDate();
            return ErrorReturn.OK();
        } catch (SQLException e) {
            return new ErrorReturn(1, e.getMessage());
        }
    }

}
