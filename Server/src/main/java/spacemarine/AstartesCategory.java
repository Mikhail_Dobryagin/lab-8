package spacemarine;

import java.io.Serializable;

/**
 * Класс, обозначающий роль(специальность) десантника
 * @author Михаил
 */
public enum AstartesCategory implements Serializable
{
    SCOUT,
    AGGRESSOR,
    TACTICAL,
    TERMINATOR,
    LIBRARIAN;
    public static final long serialVersionUID = 1L;
}
