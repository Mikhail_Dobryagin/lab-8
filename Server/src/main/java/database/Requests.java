package database;

import spacemarine.SpaceMarine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Requests
{
    final static String INSERT = "insert into spacemarines(id, name, X, Y, creation_date, health, category, weapon, melee_weapon, chapter_name, chapter_legion, login) values(nextval('id_generator'), ?, ?, ?, CURRENT_DATE, ?, ?, ?, ?, ?, ?, ?);";
    final static String UPDATE = "update spacemarines set name = ?, X = ?, Y = ?, health = ?, category = ?, weapon = ?, melee_weapon = ?, chapter_name = ?, chapter_legion = ? where login = ? and id = ?;";
    final static String REMOVE = "delete from spacemarines where login = ? and id = ?;";
    final static String CLEAR = "delete from spacemarines where login = ?;";

    private static void prepareSpacemarine(User user, PreparedStatement preparedStatement, SpaceMarine spaceMarine) throws SQLException
    {
        preparedStatement.setString(1, spaceMarine.getName());
        preparedStatement.setFloat(2, spaceMarine.getCoordinates().getX());
        preparedStatement.setLong(3, spaceMarine.getCoordinates().getY());
        preparedStatement.setDouble(4, spaceMarine.getHealth());
        preparedStatement.setString(5, spaceMarine.getCategory().toString());
        preparedStatement.setString(6, spaceMarine.getWeaponType().toString());
        preparedStatement.setString(7, spaceMarine.getMeleeWeapon().toString());
        preparedStatement.setString(8, spaceMarine.getChapter() != null ? spaceMarine.getChapter().getName() : "");
        preparedStatement.setString(9, spaceMarine.getChapter() != null ? spaceMarine.getChapter().getParentLegion() : "");
        preparedStatement.setString(10, user.getLogin());
    }

    public static PreparedStatement makeInsertStatement(Connection connection, User user, SpaceMarine spaceMarine) throws SQLException
    {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT);

        prepareSpacemarine(user, preparedStatement, spaceMarine);
        return preparedStatement;
    }

    public static PreparedStatement makeClearStatement(Connection connection, User user) throws SQLException
    {
        PreparedStatement preparedStatement = connection.prepareStatement(CLEAR);
        preparedStatement.setString(1, user.getLogin());

        return preparedStatement;
    }

    public static PreparedStatement makeRemoveStatement(Connection connection, User user, Long id) throws SQLException
    {
        PreparedStatement preparedStatement = connection.prepareStatement(REMOVE);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setLong(2, id);

        return preparedStatement;
    }

    public static PreparedStatement makeUpdateStatement(Connection connection, User user, SpaceMarine spaceMarine, Long id) throws SQLException
    {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);

        prepareSpacemarine(user, preparedStatement, spaceMarine);
        preparedStatement.setLong(11, id);

        return preparedStatement;
    }
}
