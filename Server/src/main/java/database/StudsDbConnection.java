package database;

import spacemarine.*;
import util.ColAnsi;

import java.io.Console;
import java.sql.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.PriorityQueue;

public class StudsDbConnection
{
    private Connection connection;

    public Connection getConnection()
    {
        return connection;
    }

    private String login = null;


    public void connectToStuds() throws ClassNotFoundException
    {
        Class.forName("org.postgresql.Driver");

        Console console = System.console();

        String password;

        while (login == null) {
            String wayToDb = "jdbc:postgresql://pg:5432/studs";

            {

                do {
                    System.out.println("Хотите ли вы ввести базу данных(+/-): ");

                    String yesOrNo = console.readLine();

                    if (yesOrNo == null)
                        System.exit(0);

                    if (yesOrNo.equals("-"))
                        break;

                    if (yesOrNo.equals("+")) {
                        System.out.println("Введите базу данных: ");
                        wayToDb = console.readLine();
                        break;
                    }

                    System.out.println(ColAnsi.ANSI_YELLOW + "Введите + или -" + ColAnsi.ANSI_RESET);
                } while (true);
            }
            System.out.print("Введите логин: ");
            login = console.readLine();

            if (login == null)
                System.exit(0);

            System.out.print("Введите пароль: ");
            password = new String(console.readPassword());

            if (password == null)
                System.exit(0);

            try {
                connection = DriverManager.getConnection(wayToDb, login, password);
                //connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/studs", login, password);
                connection.createStatement().executeQuery("select nextval('id_generator')");
            } catch (java.sql.SQLException e) {
                System.out.println(e.getMessage());
                login = null;
            }
        }

    }

    public String getLogin()
    {
        return login;
    }

    public boolean insertSm(User user, SpaceMarine spaceMarine) throws SQLException
    {
        Statement statement = connection.createStatement();

        ResultSet id_ResSet = statement.executeQuery("select currval('id_generator')");
        id_ResSet.next();
        spaceMarine.setId(id_ResSet.getLong("currval") + 1L);

        return Requests.makeInsertStatement(connection, user, spaceMarine).executeUpdate() != 0;
    }

    public boolean clearDB(User user) throws SQLException
    {
        return Requests.makeClearStatement(connection, user).executeLargeUpdate() != 0;
    }

    public boolean updateSm(User user, Long id, SpaceMarine spaceMarine) throws SQLException
    {
        return Requests.makeUpdateStatement(connection, user, spaceMarine, id).executeUpdate() != 0;
    }

    public boolean removeSmById(User user, Long id) throws SQLException
    {
        return Requests.makeRemoveStatement(connection, user, id).executeUpdate() != 0;
    }

    public PriorityQueue<SpaceMarineWithOwner> getSpaceMarines() throws SQLException
    {
        PriorityQueue<SpaceMarineWithOwner> subPq = new PriorityQueue<>();

        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from spacemarines;");


        while (resultSet.next())
            subPq.add(getSmFromResultSet(resultSet));

        return subPq;
    }

    public Date getCreationDate() throws SQLException
    {
        ResultSet resultSet = connection.createStatement().executeQuery("select * from creation_date;");
        resultSet.next();

        return resultSet.getDate("creation_date");
    }

    private SpaceMarineWithOwner getSmFromResultSet(ResultSet resultSet) throws SQLException
    {
        long id = resultSet.getLong("id");
        String name = resultSet.getString("name");
        Coordinates coordinates = new Coordinates(resultSet.getFloat("X"), resultSet.getLong("Y"));
        LocalDate date = resultSet.getDate("creation_date").toLocalDate();
        double health = resultSet.getDouble("health");
        AstartesCategory category = AstartesCategory.valueOf(resultSet.getString("category"));
        Weapon weapon = Weapon.valueOf(resultSet.getString("weapon"));
        MeleeWeapon meleeWeapon = MeleeWeapon.valueOf(resultSet.getString("melee_weapon"));
        Chapter chapter = !resultSet.getString("chapter_name").equals("") ? new Chapter(resultSet.getString("chapter_name"), resultSet.getString("chapter_legion")) : null;

        return new SpaceMarineWithOwner(id, name, coordinates, date, health, category, weapon, meleeWeapon, chapter, resultSet.getString("login"));
    }

    /**
     * @param user
     * @return 0 - есть такой и пароль подходит
     * 1 - есть такой и пароль НЕ подходит
     * -1 - нет такого
     * @throws SQLException
     */
    public CheckUserResult checkUser(User user) throws SQLException //0 - всё ок, 1 - не подходит пароль, -1 - нет такого
    {
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from users where login = '" + user.getLogin() + "';");
        if (!resultSet.next())
            return CheckUserResult.WRONG_USER;

        return Arrays.equals(resultSet.getBytes("password"), user.getPassword()) ? CheckUserResult.RIGHT : CheckUserResult.WRONG_PASSWORD;
    }

    public void registerUser(User user) throws SQLException
    {
        PreparedStatement preparedStatement = connection.prepareStatement("insert into users(login, password) values(?, ?);");

        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setBytes(2, user.getPassword());


        preparedStatement.executeUpdate();

    }

}
